var express = require('express');
var app = express();
var bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(express.static('public'));

app.get('/login', function (req, res) {
   res.sendFile( __dirname + "/public/html/" + "index.html" );
})

app.post('/signup', function (req, res) {
   // Prepare output in JSON format
   var obj = req.body;
   console.log(obj);
   if(!obj.subscribe){
      res.end("Hello "+obj.first_name+" "+obj.last_name+" Thank you for signing up. Your account is now created");
   }else{
      res.end("Hello "+obj.first_name+" "+obj.last_name+" Thank you for signing up. Your account is now created. You would be receiving out periodic newsletters to your email" +obj.email);
   }
})

var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
   
   console.log("Example app listening at http://%s:%s", host, port)
})